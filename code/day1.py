with open('ressource/day1.txt') as f:
    lines = f.readlines()

elfe = [0]
for line in lines:
    if line in ["","\n"]:
        elfe.append(0)
    else:
        elfe[-1] += int(line)

elfe = sorted(elfe)
elfe.reverse()

print(elfe[0])
print(sum(elfe[:3]))