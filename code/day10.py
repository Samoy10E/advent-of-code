with open("ressource/day10.txt") as f:
  lines = f.readlines()

cycle = [0]
X = [1]
milestone = []

for line in lines:
  if "noop" in line or "addx" in line:
    cycle.append(cycle[-1]+1)
  if cycle[-1]>=20+40*len(milestone):
    milestone.append(cycle[-1]*X[-1])

  if "addx" in line:
    cycle.append(cycle[-1]+1)
    if cycle[-1]>=20+40*len(milestone):
      milestone.append(cycle[-1]*X[-1])
    i,n = line.split(" ")
    X.append(X[-1]+int(n))

print(sum(milestone))

cycle = 0
X = 1
t = ""

for line in lines:
  cycle+=1

  if X<=(cycle-1)%40+1<X+3:
    t+="#"
  elif cycle%40!=0:
    t+=" "
  if cycle%40==0:
    t+="\n"

  if "addx" in line:
    cycle+=1
    if X<=(cycle-1)%40+1<X+3:
      t+="#"
    elif cycle%40!=0:
      t+=" "
    if cycle%40==0:
      t+="\n"
    i,n = line.split(" ")
    X+=int(n)


print(t)