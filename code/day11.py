class Monkey:
  
  @property
  def vide(self):
    return len(self.items)==0
    
  def __init__(self,items, binop, symb, p, a, b, n):
    self.items=items
    self.binop=binop
    self.symb=symb
    self.p=p
    self.a=a
    self.b=b
    self.n=n
  
  def op(self, item):
    if self.binop=="+":
      return item+int(self.symb)
    elif self.symb=="old":
      return item*item
    else:
      return item*int(self.symb)

  def throw(self):
    if len(self.items)==0:
      return
    item1=self.items.pop(0)
    item2=self.op(item1)
    item=item2 % 223_092_870
    return (item, self.a) if item%self.p==0 else (item, self.b)

  def receive (self, item):
    self.items.append(item)
    
  def show(self):
    print(f"Monkey {self.n}: {self.items}")
    
  def __repr__(self):      
    t=f"Monkey {self.n}:\n"
    t+=f" Starting items: {self.items}\n"
    t+=f" Operation: {self.op}\n"
    t+=f" Test: divisible by {self.p}\n"
    t+=f"  If true: throw to monkey {self.a}\n"
    t+=f"  If false: throw to monkey {self.b}\n"
    return t

with open("ressource/day11.txt") as f:
  lines=f.readlines()
  
monkeys=[]
for k in range(len(lines)//7):
  items_str=lines[7*k+1][17:-1].split(", ")
  items=[int(item) for item in items_str]
  binop=lines[7*k+2][23]
  symb=lines[7*k+2][25:-1]
  p=int(lines[7*k+3][21:-1])
  a=int(lines[7*k+4][28:-1])
  b=int(lines[7*k+5][29:-1])
  
  monkey = Monkey(items,binop, symb,p,a,b,k)
  monkeys.append(monkey)
  
N=10000
c=[0]*len(monkeys)

for _ in range(N):
  for monkey in monkeys:
    c[monkey.n]+=len(monkey.items)
    while not monkey.vide:
      item, m = monkey.throw()
      monkeys[m].receive(item)

print(c)
c=sorted(c)
print(c[-1]*c[-2])
  