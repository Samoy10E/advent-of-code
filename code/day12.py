from numpy import inf, zeros
import itertools
import numba

with open("ressource/day12.txt") as f:
    lines = f.readlines()

class Noeud:
    def __init__(self, height):
        self.adj = []
        self.height = height  
        self.value = inf

    def __repr__(self):
        return f"{self.value}"

width = len(lines[0][:-1])
length = len(lines)

noeuds = zeros(width*length, dtype=Noeud)
S = None
E = None
lesA = []

k=0
for line in lines:
    for c in line[:-1]:
        # hauteur
        if c=="S":
            S = Noeud(abs(ord("z")-ord("a")))
            noeuds[k]=S
            lesA.append(S)
        elif c=="E":
            E = Noeud(0)
            noeuds[k]=E
        else:
            noeuds[k] = Noeud(abs(ord("z")-ord(c)))
            if c=="a":
                lesA.append(noeuds[k])
        
        k+=1

noeuds = noeuds.reshape((length,width))
# S.value = 0
E.value = 0 

for i, j in itertools.product(range(length), range(width)):
    if i>0 and noeuds[(i-1),j].height-noeuds[i,j].height<=1:
        noeuds[i,j].adj.append(noeuds[(i-1),j])

    if i<length-1 and noeuds[(i+1),j].height-noeuds[i,j].height<=1:
        noeuds[i,j].adj.append(noeuds[(i+1),j])

    if j>0 and noeuds[i,j-1].height-noeuds[i,j].height<=1:
        noeuds[i,j].adj.append(noeuds[i,j-1])

    if j<width-1 and noeuds[i,j+1].height-noeuds[i,j].height<=1:
        noeuds[i,j].adj.append(noeuds[i,j+1])

def BFS(noeud_dep):
    pool = [noeud_dep]

    while pool:
        noeud = pool.pop()
        for voisin in noeud.adj:
            if voisin.value>noeud.value+1:
                voisin.value = noeud.value+1
                pool.append(voisin)

BFS(E)

# BFS(S)
print(S.value)

L = [a.value for a in lesA]
print(min(L))

# t = ""
# for i in range(length):
#     for j in range(width):
#         t += "#" if noeuds[i,j].value==inf else chr(noeuds[i,j].height)
#     t += "\n"

# print(t)
