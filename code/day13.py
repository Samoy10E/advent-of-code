from enum import Enum

with open("ressource/day13.txt") as f:
    lines = f.readlines()

"""
- both integer:
    -> l<r: GOOD
    -> l=r: OSEF, NEXT
    -> l>r: WRONG
- both list:
    PARCOURS!
    -> compare elements
    -> len(l)<len(r): GOOD
    -> len(l)=len(r): OSEF, NEXT
    -> len(l)<len(r): WRONG
- one integer:
    -> i => [i]
"""

class Etat(Enum):
    GOOD = 1
    OSEF_GO_NEXT = 2
    WRONG = 3

def both_integer(a,b):
    if a<b:
        return Etat.GOOD
    elif a==b:
        return Etat.OSEF_GO_NEXT
    else:
        return Etat.WRONG

def both_list(La, Lb):
    etat = Etat.OSEF_GO_NEXT
    k = 0
    while k<len(La) and k<len(Lb) and etat==Etat.OSEF_GO_NEXT:
        a, b = La[k], Lb[k]
        if type(a)==list and type(b)==list:
            etat = both_list(a, b)
            k += 1
        elif type(a)==int and type(b)==int:
            etat = both_integer(a, b)
            k += 1
        else:
            La[k], Lb[k] = one_int(a, b)
    
    if etat != Etat.OSEF_GO_NEXT:
        return etat

    if len(La)==len(Lb):
        return Etat.OSEF_GO_NEXT
    else:
        return Etat.GOOD if len(La)<len(Lb) else Etat.WRONG

def one_int(a,b):
    return ([a], b) if type(a)==int else (a, [b])

C = 0
for k in range(len(lines)//3+1):
    packetA = eval(lines[3*k])
    packetB = eval(lines[3*k+1])

    result = both_list(packetA.copy(),packetB.copy())
    if result==Etat.GOOD:
        C += k+1

print(C)

packets = []
for k in range(len(lines)//3+1):
    packets.extend((eval(lines[3*k]), eval(lines[3*k+1])))

k = 0
while k<len(packets):
    j = k
    while j>0 and both_list(packets[j].copy(),packets[j-1].copy())==Etat.GOOD:
        packets[j],packets[j-1] = packets[j-1],packets[j]
        j -= 1

    if j==k:
        k += 1

p1 = None
p2 = None
for k, pack in enumerate(packets):
    if p1 is None and both_list([[2]], pack.copy()) == Etat.GOOD:
        p1 = k+1
    if p2 is None and both_list([[6]], pack.copy()) == Etat.GOOD:
        p2 = k+2

print(p1*p2)