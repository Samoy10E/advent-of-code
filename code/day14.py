from numpy import array, zeros

with open("ressource/day14.txt") as f:
    lines = f.readlines()

def create_line(X,Y,coord1, coord2):
    x1,y1 = coord1
    x2,y2 = coord2

    if x1==x2:
        while y1!=y2:
            X.append(x1)
            Y.append(y1)
            y1 += (-1)**(y1>y2)
    else:
        while x1!=x2:
            X.append(x1)
            Y.append(y1)
            x1 += (-1)**(x1>x2)
    return

def print_sable(START, END, OCCUPE, MUR):
    t = ""
    for j in range(MUR.shape[1]):
        for i in range(START, END):
            if not OCCUPE[i,j]:
                t += "."
            elif not MUR[i,j]:
                t += "o"
            else:
                t += "#"
        t += "\n"

    print(t)

X, Y = [], []

for line in lines:
    Tcoords = line.split(" -> ")

    coords = []
    for Tcoord in Tcoords:
        x,y = Tcoord.split(",")
        coords.append((int(x),int(y)))
    
    for k in range(len(coords)-1):
        create_line(X, Y, coords[k], coords[k+1])
    
    X.append(coords[-1][0])
    Y.append(coords[-1][1])

MUR = zeros((max(X)+max(Y)+4,max(Y)+3), dtype=bool)

for k in range(len(X)):
    MUR[X[k],Y[k]] = True

START, END = 500-max(Y)-1, 500+max(Y)+2
OCCUPE = MUR.copy()
# print_sable(START, END, OCCUPE, MUR)

def sim1(OCCUPE, Nsable = 0):
    coord = array([500,0])
    for _ in range(max(Y)):
        if not OCCUPE[coord[0],coord[1]+1]:
            coord = coord+[0,1]
        elif not OCCUPE[coord[0]-1,coord[1]+1]:
            coord = coord+[-1,1]
        elif not OCCUPE[coord[0]+1,coord[1]+1]:
            coord = coord+[1,1]
        else:
            OCCUPE[coord[0],coord[1]] = True
            break
    else:
        return Nsable

    return sim1(OCCUPE, Nsable+1)

print(sim1(OCCUPE))

print_sable(START, END, OCCUPE, MUR)

MUR[:,-1] = True
OCCUPE = MUR.copy()

def sim2(OCCUPE):
    Nsable = 0
    while not OCCUPE[500,0]:
        coord = array([500,0])
        for _ in range(max(Y)+2):
            if not OCCUPE[coord[0],coord[1]+1]:
                coord = coord+[0,1]
            elif not OCCUPE[coord[0]-1,coord[1]+1]:
                coord = coord+[-1,1]
            elif not OCCUPE[coord[0]+1,coord[1]+1]:
                coord = coord+[1,1]
            else:
                OCCUPE[coord[0],coord[1]] = True
                break
        Nsable = Nsable+1

    return Nsable


print(sim2(OCCUPE))

print_sable(START, END, OCCUPE, MUR)