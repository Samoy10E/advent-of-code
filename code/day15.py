#!/usr/bin/python
import threading
import itertools
import sys
from rich.progress import track, Progress

with open("ressource/day15.txt") as f:
    lines = f.readlines()

def dist_taxicab(coord1, coord2):
    return abs(coord1[0]-coord2[0])+abs(coord1[1]-coord2[1])

balises = set()
sensors = []

for line in lines:
    sp = line.split(" ")
    coord1, coord2 = sp[2:4], sp[8:10]
    coord1 = [int(coord1[0][2:-1]),int(coord1[1][2:-1])]
    coord2 = (int(coord2[0][2:-1]),int(coord2[1][2:-1]))
    sensors.append((coord1, dist_taxicab(coord1, coord2)))
    balises.add(coord2)

# print(balises)
# print(sensors)

def VU(sensor, Y):
    coord, d = sensor
    k = abs(Y-coord[1])
    x0 = coord[0]-(d-k)
    for i in range(2*(d-k)+1):
        yield (x0+i,Y)

# monY = 2000000
# vues = set()
# for sensor in track(sensors, description="Processing..."):
#     for caseVu in VU(sensor, monY):
#         if caseVu not in balises:
#             vues.add(caseVu)

# print(len(vues))

sensors = sorted(sensors, key=lambda x:x[1])
sensors.reverse()

def next_el(x,y):
    skip = 0
    for k in range(len(sensors)):
        coordS, d = sensors[k]
        k = dist_taxicab(coordS,(x,y))
        if d-k+1>skip:
            return d-k+1
    return skip

dim = 2000000

Nthread = 1

with Progress() as progress:

    def cherche(x,y,dimX,dimY,nT,task):
        x0,y0 =x,y
        dist = next_el(x,y)
        while dist!=0 and y<=dimY:
            progress.update(task, advance=dist, completed=False)
            x += dist
            if x>dimX:
                x = x0
                y += 1
            dist = next_el(x,y)

        if y<=dimY:
            print(f"\033[92m\u2713 REUSSITE n°{nT} : {x*4000000+y}\033[0m")
        else:
            print(f"\033[91m\u2715 ECHEC n°{nT}\033[0m")

        progress.update(task, completed=True)

    try:
        for i, j in itertools.product(range(Nthread), range(Nthread)):
            l = dim//Nthread
            task = progress.add_task(f"Tache n°{i*Nthread+j+1}", total=l*l)
            threading.Thread(target=cherche, args=(i*l,j*l,(i+1)*l-1,(j+1)*l-1, i*Nthread+j,task)).start()

    except Exception:
        print("Error: unable to start thread")
