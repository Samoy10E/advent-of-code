from numpy import False_, zeros, inf, array

with open("ressource/day16.txt") as f:
    lines = f.readlines()

Nvalve = len(lines)

nomValve = {}
adjL = [[] for _ in range(Nvalve)]
rateValve = [0]*Nvalve

for line in lines:
    Split_line = line.split(" ")
    nom = Split_line[1]
    rate = Split_line[4]
    voisins = Split_line[9:]
    if nom not in nomValve:
        nomValve[nom] = len(nomValve)

    rateValve[nomValve[nom]] = int(rate[5:-1])

    for voisin in voisins:
        if voisin[:-1] not in nomValve:
            nomValve[voisin[:-1]] = len(nomValve)
        adjL[nomValve[nom]].append(nomValve[voisin[:-1]])

Dvalve = zeros((Nvalve,Nvalve), dtype=int) + 10**9

for i in range(Nvalve):
    Dvalve[i,i] = 0
    stack = [i]
    while stack:
        valve = stack.pop()
        for voisin in adjL[valve]:
            if Dvalve[i,voisin]>Dvalve[i,valve]+1:
                stack.append(voisin)
                Dvalve[i,voisin] = Dvalve[i,valve]+1

coutValve = zeros((Nvalve, Nvalve))
for i in range(Nvalve):
    coutValve[:,i] = rateValve[i]
coutValve = coutValve/(Dvalve+1)

closeValve = [True]*Nvalve
T = 30
released = 0
valve = 0
t="0"
while T>0:
    # print(valve, T, released)
    new_valve = valve
    for j in range(Nvalve):
        if coutValve[valve,new_valve]*closeValve[new_valve]<=coutValve[valve,j]*closeValve[j] and T-(Dvalve[valve,j]+1)>0:
            new_valve = j
    T -= Dvalve[valve,new_valve]+1
    released += rateValve[new_valve]*T*closeValve[new_valve]
    closeValve[new_valve] = False
    valve = new_valve
    t += f"-{new_valve}"

# print(valve, T, released)
maxRelease = released
print(maxRelease,t)

closeValve = array([True]*Nvalve)
for n in range(Nvalve):
    if rateValve[n]==0:
        closeValve[n] = False

T = 30
def best_released(valve, released, closeValve, T, R, t="0"):

    if T<=0 or True not in closeValve and maxRelease<=released:
        R.append((released,t))
        print(t, released)
        return 
    
    bouge = False
    T -= 1
    for new_valve in range(Nvalve):
        if T-Dvalve[valve,new_valve]>=1 and closeValve[new_valve]:
            bouge = True
            Tp = T-Dvalve[valve,new_valve]
            releasedp = released + rateValve[new_valve]*Tp*closeValve[new_valve]
            closeValvep = closeValve.copy()
            closeValvep[new_valve] = False
            best_released(new_valve, releasedp, closeValvep, Tp, R, f"{t}-{str(new_valve)}")

    if not bouge and maxRelease<=released:
        print(t, released)
        R.append((released,t))
        return 


R = []
best_released(0, 0, closeValve.copy(), 30, R)
print(max(R, key=lambda x:x[0]))
