with open('ressource/day2.txt') as f:
    lines = f.readlines()

Vrps = [1, 2, 3]
Vltw = [0, 3, 6]
s = [1,0,2]
possible_outcome = {}

for i,d in enumerate(["A", "B", "C"]):
    for j,w in enumerate(["X", "Y", "Z"]):
        possible_outcome[f"{d} {w}"] = Vltw[(s[i]+j)%3] + Vrps[j]

print(sum(possible_outcome[line[:3]] for line in lines))

for i,d in enumerate(["A", "B", "C"]):
    for j,w in enumerate(["X", "Y", "Z"]):
        possible_outcome[f"{d} {w}"] = Vltw[j] + Vrps[(i-s[j])%3]

print(sum(possible_outcome[line[:3]] for line in lines))
