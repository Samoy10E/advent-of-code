def conv(c):
    if ord(c) > 96:
        return ord(c) - 96
    return ord(c) - 64 + 26


with open("ressource/day3.txt") as f:
  lines = f.readlines()

print("#lines : ", len(lines))
count = 0
for line in lines:
  n = len(line) - 1
  l1, l2 = line[:n // 2], line[n // 2:n]

  l1 = [conv(c) for c in l1]
  l2 = [conv(c) for c in l2]
  l1 = sorted(l1)
  l2 = sorted(l2)
  i, j = 0, 0
  while l1[i] != l2[j]:
    if l1[i] < l2[j]:
      i += 1
    else:
      j += 1

  count += l1[i]

print("count : ", count)

count = 0
for k in range(0, len(lines), 3):
  l1 = [conv(c) for c in lines[k] if c != "\n"]
  l2 = [conv(c) for c in lines[k + 1] if c != "\n"]
  l3 = [conv(c) for c in lines[k + 2] if c != "\n"]
  #print(lines[k])
  #print(lines[k+1])
  #print(lines[k+2])
  l1 = sorted(l1)
  l2 = sorted(l2)
  l3 = sorted(l3)
  i, j, p = 0, 0, 0
  while l1[i] != l2[j] or l1[i] != l3[p]:
    if l1[i] <= l2[j] and l1[i] <= l3[p]:
      i += 1
    elif l2[j] <= l1[i] and l2[j] <= l3[p]:
      j += 1
    else:
      p += 1
  #print(l1[i])
  count += l1[i]

print("count : ", count)
