import copy
with open("ressource/day5.txt") as f:
    lines = f.readlines()

Piles = [[] for _ in range(9)]

for i in range(8):
    line = lines[i]
    for k in range(9):
        if line[4*k+1]!=" ":
            Piles[k].append(line[4*k+1])

for k in range(9):
    Piles[k].reverse()

    
pilesA = copy.deepcopy(Piles)
for line in lines[10:]:
    MOVE, nb, FROM, pile1, TO, pile2 = line[:-1].split(" ")
    for _ in range(int(nb)):
        pilesA[int(pile2)-1].append(pilesA[int(pile1)-1].pop())

t = "".join(pilesA[k].pop() for k in range(9))
print(t)


pilesB = copy.deepcopy(Piles)
pile_temp = []
for line in lines[10:]:
    MOVE, nb, FROM, pile1, TO, pile2 = line[:-1].split(" ")
    pile_temp.clear()
    pile_temp.extend(pilesB[int(pile1)-1].pop() for _ in range(int(nb)))
    pile_temp.reverse()
    pilesB[int(pile2)-1] = pilesB[int(pile2)-1]+ pile_temp
    
t = "".join(pilesB[k].pop() for k in range(9))
print(t)