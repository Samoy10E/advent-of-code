with open("ressource/day6.txt") as f:
    lines = f.readlines()[0]

D = 14

l = []
for k in range(D):
    if lines[k] not in l:
        l.append(lines[k])
count = len(l)

k = D
while count!=D and k<len(lines):
    if lines[k-D] in lines[k-D+1:k]:
        count += 1
    if lines[k] in lines[k-D+1:k]:
        count -= 1
    k += 1

print(k,lines[k-D:k])