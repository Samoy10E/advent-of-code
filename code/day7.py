with open("ressource/day7.txt") as f:
    lines = f.readlines()

class Noeud:

    @property
    def estFeuille(self):
        return len(self.enfants)==0

    def __init__(self, parent, nom, valeur=0):
        self.parent = parent
        self.nom = nom
        self.valeur = valeur
        self.enfants = []

        if parent!=None:
            parent.ajout_enfant(self)

    def ajout_enfant(self, noeud):
        self.enfants.append(noeud)
        self.valeur += noeud.valeur
        if self.parent != None:
            self.parent.ajout_valeur(noeud.valeur)

    def ajout_valeur(self, valeur):
        self.valeur += valeur
        if self.parent != None:
            self.parent.ajout_valeur(valeur)

    def __repr__(self):
        t = f"Nom : {self.nom}\n"
        t += f"Valeur : {self.valeur}\n"
        t += f"Est une feuille : {self.estFeuille}\n"
        t += "Enfant:\n"
        for n in self.enfants:
            t += f"\t-{n.nom} (valeur={n.valeur})\n"
        return t

root = Noeud(None, "/")
noeud_courant = root

for line in lines:
    if line[0]=="$":
        if line[2]!="c":
            continue

        if line[5]=="/":
            noeud_courant = root
        elif line[5]==".":
            noeud_courant = noeud_courant.parent
        else:
            t = line[5:-1]
            for enfant in noeud_courant.enfants:
                if enfant.nom == t:
                    noeud_courant = enfant
                    break
    else:
        t1, t2 = line[:-1].split(" ")
        if t1=="dir":
            Noeud(noeud_courant, t2)
        else:
            Noeud(noeud_courant, t2, int(t1))

c = 0
L = root.enfants
P = [root.valeur]
print(root)
while len(L)!=0:
    noeud = L.pop()
    if not noeud.estFeuille:
        L.extend(noeud.enfants)

        if noeud.valeur<100000:
            c += noeud.valeur

        if noeud.valeur>root.valeur-40_000_000:
            P.append(noeud.valeur)

print(c)

P = sorted(P)
print(P)
