import itertools
import numpy as np
with open("ressource/day8.txt") as f:
    lines = f.readlines()

n = len(lines)

FORET = np.zeros((n,n), dtype=int)
for i,line in enumerate(lines):
    for j,a in enumerate(line[:-1]):
        FORET[i,j] = int(a)

ARBRE_VU = np.zeros((n,n), dtype=bool)

nb_arbre_vu = 0
for j in range(n):
    t_arbre = FORET[0,j]
    for i in range(n):
        if i==0:
            if not ARBRE_VU[i,j]:
                nb_arbre_vu += 1
            ARBRE_VU[i,j] = True
        elif FORET[i,j]>t_arbre:
            if not ARBRE_VU[i,j]:
                ARBRE_VU[i,j] = True
                nb_arbre_vu += 1
            t_arbre = FORET[i,j]


for j in range(n):
    t_arbre = FORET[n-1,j]
    for i in range(n):
        if i==0:
            if not ARBRE_VU[n-1-i,j]:
                nb_arbre_vu += 1
            ARBRE_VU[n-1-i,j] = True
        elif FORET[n-1-i,j]>t_arbre:
            if not ARBRE_VU[n-1-i,j]:
                ARBRE_VU[n-1-i,j] = True
                nb_arbre_vu += 1
            t_arbre = FORET[n-1-i,j]

for i in range(n):
    t_arbre = FORET[i,0]
    for j in range(n):
        if j==0:
            if not ARBRE_VU[i,j]:
                nb_arbre_vu += 1
            ARBRE_VU[i,j] = True
        elif FORET[i,j]>t_arbre:
            if not ARBRE_VU[i,j]:
                ARBRE_VU[i,j] = True
                nb_arbre_vu += 1
            t_arbre = FORET[i,j]

for i in range(n):
    t_arbre = FORET[i,n-1]
    for j in range(n):
        if j==0:
            if not ARBRE_VU[i,n-1-j]:
                nb_arbre_vu += 1
            ARBRE_VU[i,n-1-j] = True
        elif FORET[i,n-1-j]>t_arbre:
            if not ARBRE_VU[i,n-1-j]:
                ARBRE_VU[i,n-1-j] = True
                nb_arbre_vu += 1
            t_arbre = FORET[i,n-1-j]

# print(ARBRE_VU,nb_arbre_vu)

BEST_SPOT = np.zeros((n,n), dtype=int)+1

for i, j in itertools.product(range(n), range(n)):
    t_arbre = -1
    k = j+1
    s = 0
    while k<n:
        s += 1
        if not FORET[i, j] > FORET[i, k]:
            break
        if FORET[i,k] > t_arbre:
            t_arbre = FORET[i,k]
        k += 1

    BEST_SPOT[i,j] *= s

    t_arbre = -1
    k = j-1
    s = 0
    while k>=0:
        s += 1
        if not FORET[i, j] > FORET[i, k]:
            break
        if FORET[i,k] > t_arbre:
            t_arbre = FORET[i,k]
        k -= 1

    BEST_SPOT[i,j] *= s

    t_arbre = -1
    k = i+1
    s = 0
    while k<n:
        s += 1
        if not FORET[i, j] > FORET[k, j]:
            break
        if FORET[k,j] > t_arbre:
            t_arbre = FORET[k,j]
        k += 1

    BEST_SPOT[i,j] *= s

    t_arbre = -1
    k = i-1
    s = 0
    while k>=0:
        s += 1
        if not FORET[i, j] > FORET[k, j]:
            break
        if FORET[k,j]:
            t_arbre = FORET[k,j]
        k -= 1
        
    BEST_SPOT[i,j] *= s


print(BEST_SPOT)
print(BEST_SPOT.max())