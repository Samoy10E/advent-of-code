import numpy as np

with open("ressource/day9.txt") as f:
    lines = f.readlines()

D = {(2,0) : np.array([1,0], dtype=int),
    (2,1) : np.array([1,1], dtype=int),
    (2,2) : np.array([1,1], dtype=int),
    (1,2) : np.array([1,1], dtype=int),
    (0,2) : np.array([0,1], dtype=int),
    (-1,2) : np.array([-1,1], dtype=int),
    (-2,2) : np.array([-1,1], dtype=int),
    (-2,1) : np.array([-1,1], dtype=int),
    (-2,0) : np.array([-1,0], dtype=int),
    (-2,-1) : np.array([-1,-1], dtype=int),
    (-2,-2) : np.array([-1,-1], dtype=int),
    (-1,-2) : np.array([-1,-1], dtype=int),
    (0,-2) : np.array([0,-1], dtype=int),
    (1,-2) : np.array([1,-1], dtype=int),
    (2,-2) : np.array([1,-1], dtype=int),
    (2,-1) : np.array([1,-1], dtype=int),
    }


N = 10
corde = [np.zeros(2, dtype=int) for _ in range(N)]

chemin = [tuple(corde[-1])]

for line in lines:
    direction, n = line.split(" ")

    for _ in range(int(n)):
        if direction=="U":
            corde[0] += [0,1]
        elif direction=="R":
            corde[0] += [1,0]
        elif direction=="D":
            corde[0] += [0,-1]
        else:
            corde[0] += [-1,0]

        for k in range(N-1):
            dep = D.get(tuple(corde[k]), np.zeros(2, dtype=int))
            corde[k] -= dep
            corde[k+1] += dep

        if tuple(corde[-1]) not in chemin:
            chemin.append(tuple(corde[-1])) 

print(len(chemin))