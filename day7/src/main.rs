pub mod tree;
use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let fichier = lecture_fichier(7)?;
    // println!("{}", fichier);
    lecture_commande(fichier);
    Ok(())
}

fn lecture_fichier(jour: u32) -> std::io::Result<String> {
    // Lecture du fichier du jour
    let nom = format!("../ressource/day{}.txt",jour);
    println!("{}", nom);
    let mut file = File::open(nom)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    Ok(contents)
}

fn lecture_commande(fichier: String){
    let mut _root = tree::TreeNode()::new();
    let noeud = root;
    let _size = fichier.len() as u32;

    let mut k = 0;
    while k<10{
        if fichier.as_bytes()[k] == '$' as u8{
            k += 2;
            if fichier.as_bytes()[k] == 'c' as u8{
                println!("cd");
                k += 2
                if fichier.as_bytes()[k] == '/' as u8{
                    noeud = root;
                }
                else if fichier.as_bytes()[k] == '.' as u8{
                    noeud = noeud.parent;
                    k += 1;
                }
                else{
                    noeud = noeud.children
                }

            }
            else{
                println!("ls");
            }
        }
        k += 1;
    }
}