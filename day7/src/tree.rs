use std::cell::RefCell;
use std::rc::Rc;

#[derive(PartialEq)]
pub struct TreeNode {
  pub value: Option<u32>,
  pub children: Vec<Rc<RefCell<TreeNode>>>,
  pub parent: Option<Rc<RefCell<TreeNode>>>,
}

impl TreeNode {
  pub fn new() -> TreeNode {
    return TreeNode {
      value: None,
      children: vec![],
      parent: None,
    };
  }

  pub fn add_child(&mut self, new_node: Rc<RefCell<TreeNode>>) {
    self.children.push(new_node);
  }

  pub fn print(&self) -> String {
    if let Some(value) = self.value {
      return value.to_string();
    } else {
      return String::from("[")
        + &self
          .children
          .iter()
          .map(|tn| tn.borrow().print())
          .collect::<Vec<String>>()
          .join(",")
        + "]";
    }
  }
}
